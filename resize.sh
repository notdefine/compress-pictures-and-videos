#!/bin/bash

# Thomas Eimers 22.07.2008 - 2019

# @see README.md

# Limit to n Megapixel
PIXEL_AREA_COUNT_LIMIT_MP=10

function checkDependencies() {
  command -v ffmpeg >/dev/null 2>&1 || {
    echo >&2 "I require ffmpeg but it's not installed.  Aborting."
    exit 1
  }
  command -v mogrify >/dev/null 2>&1 || {
    echo >&2 "I require mogrify but it's not installed.  Aborting."
    exit 1
  }
}

function getProcessorCount() {
  return "$(grep ^cpu\\scores /proc/cpuinfo | uniq | awk '{print $4}')"
}

# Warten bis die Anzahl der Backgoundjobs < als Anzahl CPUs ist
function limitBackgroundJobs() {
  while [ "$(jobs | wc -l)" -ge "$1" ]; do
    sleep 0.3
  done
}

checkDependencies

# Anzahl der dem System zur verfügung stehenden Prozessoren

OIFS="$IFS"
IFS=$'\n'

echo "Convert Quick time .mov to H264 .avi"

for videoFile in $(find . -type f -name "*.[mM][Oo][vV]"); do
  ffmpeg -i "${videoFile}" -preset slow -crf 21 -acodec libmp3lame -vcodec libx264 -hide_banner "${videoFile/[mM][oO][vV]/avi}"
  rm "${videoFile}"
done
echo ".mov files completed"

PIXEL_AREA_COUNT_LIMIT=$(expr "$PIXEL_AREA_COUNT_LIMIT_MP" \* 1000000)
echo "Resize Image if not already done"
for imageFilename in $(find . -type f -name "*.[jJ][pP][gG]" -o -name "*.jpeg"); do

  width=$(identify -format "%w" ${imageFilename})
  height=$(identify -format "%h" ${imageFilename})

  SIZE=$(expr "$width" \* "$height" / 1000000)

  if [ "$SIZE" -gt "$PIXEL_AREA_COUNT_LIMIT_MP" ]; then
    echo -n $SIZE Megapixel: Compress "$imageFilename"
    mogrify -resize "${PIXEL_AREA_COUNT_LIMIT}@" ${imageFilename}

    width=$(identify -format "%w" ${imageFilename})
    height=$(identify -format "%h" ${imageFilename})

    SIZE=$(expr "$width" \* "$height" / 1000000)
    echo " ... now $SIZE Megapixel"
  else
    echo $SIZE Megapixel: Skip file "${imageFilename}"
    continue
  fi

done

echo "Warte auf beendigung..."
# Es gibt mehrere Background Jobs, den Benutzer anzeigen wann diese sich alle
# beendet haben.
wait

echo "Rename Files"

for imageFilename in $(find . -regex '.*/[C]+IMG[0-9]*\.[Jj][Pp][Gg]' -type f); do
  directory=$(dirname "${imageFilename}")
  newFilename="$(identify -format "%[EXIF:DateTime]_%f" "${imageFilename}")"
  echo ${imageFilename} -\> ${directory}/${newFilename}
  mv ${imageFilename} "${directory}/${newFilename}"
done

IFS="$OIFS"

echo "Done"
