# compress-pictures-and-videos

Dieses Skript konvertiert/komprimiert Bilder und Videos von z.B. einer Digitalkamera wie die Casio S10, Nikon d7200 oder Photos und Videos vom Handy auf eine handhabbare Dateigröße.
## Bilder
Die Bilder werden in eine größe von ~5MP konvertiert, was für den Druck als DIN A3 ausreicht. Die Auflösung kann im Skript gegebenenfalls angepasst werden.

Diese Skript ist für Mehrprozessorsysteme ausgelegt, und erzeugt für jeden einzelnen convert Vorgang einen Background Job.

## Videos

Die Videos behalten ihre originale Auflösung, werden aber in das platzsparende Format H264 als xxx.avi umgewandelt.

## Vorteile

- Bilder werden nur runter gerechnet, wenn das Bild größer ist als die gewünschte Auflösung.
- Das Skript kann ohne Probleme mehrfach aufgerufen werden, z.B. wenn neue Bilder dazugekommen sind.
- Bilder welche nur CIMG... oder IMG_... heißen werden umbenannt und enthalten danach das Datum.
- Dateienamen dürfen auch Leerzeichen enthalten
- Das Skript durchläuft auch Unterverzeichnisse (rekursiv).
